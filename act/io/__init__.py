"""
===============
act.io (act.io)
===============

.. currentmodule:: act.io

This module contains procedures for reading and writing various ARM datasets.

.. autosummary::
    :toctree: generated/

    armfiles.ARMStandardsFlag
    armfiles.check_arm_standards
    armfiles.create_obj_from_arm_dod
    armfiles.read_netcdf
    csvfiles.read_csv
"""

from . import armfiles
from . import csvfiles
